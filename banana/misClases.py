from os import system

class Movimiento:
    def __init__(self): 
        self.FLAG_ANGULO_UMBRAL_POSITIVO = False
        self.FLAG_ANGULO_UMBRAL_NEGATIVO = False
        self.FLAG_ANGULO_UMBRAL_FLAT = False

    def izquierda(self):
        system("xdotool keydown a")
        system("xdotool keyup d")
        self.FLAG_ANGULO_UMBRAL_POSITIVO = True
        self.FLAG_ANGULO_UMBRAL_NEGATIVO = False
        self.FLAG_ANGULO_UMBRAL_FLAT = False

    def derecha(self):
        system("xdotool keydown d")
        system("xdotool keyup a")
        self.FLAG_ANGULO_UMBRAL_POSITIVO = False
        self.FLAG_ANGULO_UMBRAL_NEGATIVO = True
        self.FLAG_ANGULO_UMBRAL_FLAT = False

    def recto(self):
        system("xdotool keyup d")
        system("xdotool keyup a")
        self.FLAG_ANGULO_UMBRAL_POSITIVO = False
        self.FLAG_ANGULO_UMBRAL_NEGATIVO = False
        self.FLAG_ANGULO_UMBRAL_FLAT = False
