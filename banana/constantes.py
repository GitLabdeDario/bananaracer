import numpy as np
import cv2

amarillo_RGB = np.uint8([[[0,255,255]]])
amarillo_HSV = cv2.cvtColor(amarillo_RGB, cv2.COLOR_BGR2HSV)

amarilloBajo = np.array([amarillo_HSV[0,0,0]-10,100,20],np.uint8)
amarilloAlto = np.array([amarillo_HSV[0,0,0]+10,255,255],np.uint8)

AREA_MINIMA = 3000
ANGULO_UMBRAL_POSITIVO = 20
ANGULO_UMBRAL_NEGATIVO = -20