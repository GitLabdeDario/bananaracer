#----------- Librerías utilizadas -----------#
import cv2
import numpy as np
from os import system
from constantes import *
from misClases import *

#------------ Captura de video ------------#
captura = cv2.VideoCapture(0)
system("echo dgs | sudo -S xset r rate 2") # Elimina delay después de apretar una tecla
while (captura.isOpened()):
    ret, imagen = captura.read()
    if ret:
        frameHSV = cv2.cvtColor(imagen, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(frameHSV, amarilloBajo, amarilloAlto)
        contornos,_ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        for contorno in contornos:
            area = cv2.contourArea(contorno)
            if area > AREA_MINIMA:
                contorno_mas_grande = max(contornos, key=cv2.contourArea)   # Descarta espureos
                [vx, vy, x, y] = cv2.fitLine(contorno_mas_grande, cv2.DIST_L2, 0, 0.01, 0.01) # Aproximo contorno por una línea
                angulo_radianes = np.arctan2(vy, vx)
                angulo_grados = np.degrees(angulo_radianes)
                
                contorno_suavizado = cv2.convexHull(contorno_mas_grande)
                cv2.drawContours(imagen,[contorno_suavizado], 0, (0,0,255),3)

                movimiento = Movimiento()
                if((angulo_grados > ANGULO_UMBRAL_POSITIVO) and (not movimiento.FLAG_ANGULO_UMBRAL_POSITIVO)):
                    movimiento.izquierda()
                if((angulo_grados < ANGULO_UMBRAL_NEGATIVO) and (not movimiento.FLAG_ANGULO_UMBRAL_NEGATIVO)):
                    movimiento.derecha()
                if(((angulo_grados > ANGULO_UMBRAL_NEGATIVO) and (angulo_grados < ANGULO_UMBRAL_POSITIVO)) and (not movimiento.FLAG_ANGULO_UMBRAL_FLAT)):
                    movimiento.recto()

        cv2.imshow('video', imagen)

        if cv2.waitKey(1) & 0xFF == ord('s'):
            break
    else: break

captura.release()
system("echo dgs | sudo -S xset r rate 500")
cv2.destroyAllWindows()